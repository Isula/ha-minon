#!/usr/bin/env ruby
# haproxy-generator.rb 11/15 Stephanie Sunshine <stephanie@isula.net>

require ‘rubygems‘
require ‘bundler/setup‘

Bundler.require

#Stdlib
require 'ostruct'	# OpenStruct
require 'erb'		# ERB Templating

# We want to fail if they don't exist
sites_dir = Dir.entries("sites.d")
backends_dir = Dir.entries("backends.d")
haproxy_template = File.open("haproxy.cfg.erb", "r").read

# Cleanup arrays
sites_dir.shift 2
backends_dir.shift 2

# Show the data we are working with
puts "Sites:\n"
puts sites_dir.inspect
puts "\n"
puts "Backends:\n"
puts backends_dir.inspect

# Prep for ERB
@sites = Array.new
@backends = backends_dir

sites_dir.each do |site|
  temp_site = OpenStruct.new
  temp_site.fqdn  = site
  temp_site.fe    = site.gsub(/\./, '-') + '-f'
  temp_site.be    = site.gsub(/\./, '-') + '-b'
  @sites.push temp_site
end

puts "\n"
# Render haproxy.conf.erb as haproxy.conf
File.open('haproxy.cfg', 'w') {|f| f.write ERB.new(haproxy_template).result(binding)}

puts "Your new config is in #{Dir.pwd}/haproxy.cfg\n"
